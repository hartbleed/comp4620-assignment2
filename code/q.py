from random import random, choice
from typing import Optional

from MDP import Action, State
from domain import RLEnvironment


class Q:
    """
      A Q class is a class that represents an estimate of a pair state/action.
      Different implementations are proposed below
    """

    def __init__(self):
        pass

    def value(self, state: State, action: Action) -> float:
        """
          The value associated with the specified pair state/action.
          Not implemented in this class.
        """
        pass

    def learn(self, state: State, action: Action, reward: float) -> None:
        """
          Tells this Q that the last execution of action in state
          led to the specified (long-term) reward.
        """
        pass


class LastValueQ(Q):
    """
      An example implementation of a Q.
      The value of a pair state is the value of the last execution of that state pair,
      and 0 if it has never been applied.
    """

    def __init__(self, default: Optional[float] = 0):
        self.default_ = default
        self.values_ = {}

    def value(self, state: State, action: Action) -> float:
        if (state, action) in self.values_:
            return self.values_[state, action]
        return self.default_

    def learn(self, state: State, action: Action, reward: float) -> None:
        self.values_[state, action] = reward


class Average(Q):
    """
      An implementation of Q where the value of a pair state/action is the average
      of the value received so far (default, if there is no value).
    """

    def __init__(self, default: Optional[float] = 0):
        self.default_ = default
        self.values_ = {}

    def value(self, state: State, action: Action) -> float:
        return sum(self.values_.get((state, action), [self.default_])) / len(self.values_.get((state, action), [0]))

    def learn(self, state: State, action: Action, reward: float) -> None:
        self.values_[state, action] = self.values_.get((state, action), []) + [reward]


class TemporalDifference(Q):
    """
      An implementation of Q where the value of a pair state/action is updated
      using temporal difference (default value is 0).
    """

    def __init__(self, alpha: float, default: Optional[float] = 0):
        self.default_ = default
        self.alpha_ = alpha
        self.values_ = {}

    def value(self, state: State, action: Action) -> float:
        return self.values_.get((state, action), self.default_)

    def learn(self, state: State, action: Action, reward: float) -> None:
        delta = reward - self.values_.get((state, action), self.default_)
        self.values_[state, action] = self.values_.get((state, action), self.default_) + self.alpha_ * delta


'''
  Generic methods for Q
'''


def greedy_action(q: Q, env: RLEnvironment) -> Action:
    """
      Computes the greedy action, according to q,
      for the current state in the environment.
    """
    best_val = 0
    best_act = None
    for act in env.applicable_actions():
        val = q.value(env.current_state(), act)
        if val > best_val or best_act == None:
            best_val = val
            best_act = act
    return best_act


def epsilon_greedy_action(q: Q, env: RLEnvironment, epsilon: float) -> Action:
    """
      Computes the greedy action, according to q,
      for the current state in the environment.
    """
    if random() > epsilon:
        best_val = 0
        best_act = None
        for act in env.applicable_actions():
            val = q.value(env.current_state(), act)
            if val > best_val or best_act == None:
                best_val = val
                best_act = act
        return best_act
    return choice(env.applicable_actions())

# eof
