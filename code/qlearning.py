from typing import Optional
from copy import deepcopy

from domain import RLEnvironment
from q import Q, TemporalDifference, epsilon_greedy_action, greedy_action

class QLearning:
    '''
      An implementation of Q learning.
    '''
    def __init__(self, domain: RLEnvironment, epsilon: float, gamma: float, q: Optional[Q] = TemporalDifference(alpha=.01)):
        self.domain_ = domain
        self.epsilon_ = epsilon
        self.gamma_ = gamma
        self.q_ = q
        # I guess calculate alpha?
        


    def reset(self):
        """
          Resets the domain to its initial state
        """
        self.domain_.reset()


    def execute_one_action(self):
        '''
          Executes one action according to the QLearning strategy, and updates the Q value function.
        '''
        state = self.domain_.current_state()
        act = epsilon_greedy_action(self.q_, self.domain_, self.epsilon_)
        out = self.domain_.execute(act)
        act_p = greedy_action(self.q_, self.domain_)
        delta = out.reward + self.gamma_ * self.q_.value(out.state, act_p)
        self.q_.learn(state, act, delta)


# eof