from typing import List, Tuple

from rewardwrapper import RewardWrapper
from sarsa import Sarsa
from qlearning import QLearning
from domain import RLEnvironment
from q import Q, TemporalDifference
from cheatingcricket import cheating_cricket_team_2
import sys

def write_rewards(filename: str, datum: Tuple[List[float], str]) -> None:
    with open(filename, 'w') as file:
        file.write("{}\n".format(datum[1]))
        for value in datum[0]:
            file.write('{}\n'.format(value))

def read_rewards(filename: str) -> Tuple[List[float], str]:
    name: str
    list: List[float] = []
    with open(filename, 'r') as file:
        name = file.readline().rstrip()
        for line in file:
            list.append(float(line))
    return list, name

def basic_qlearning(domain: RLEnvironment, alpha: float, gamma: float, epsilon: float, length: int) -> Tuple[List[float],str]:
    wrap: RewardWrapper = RewardWrapper(domain)
    q: Q = TemporalDifference(alpha=alpha)
    ms: QLearning = QLearning(wrap, epsilon=epsilon, gamma=gamma, q=q)
    ms.reset()
    for _i in range(length):
        if _i % int(length / 10) == 0:
            print('+', end='', flush=True)
        elif _i % int(length / 100) == 0:
            print('.', end='', flush=True)
        ms.execute_one_action()
    print()
    return wrap.get_reward_summary(gamma), "Q-Learning with epsilon={}".format(epsilon)

def basic_sarsa(domain: RLEnvironment, alpha:float, gamma: float, epsilon: float, length: int) -> Tuple[List[float],str]:
    wrap: RewardWrapper = RewardWrapper(domain)
    q: Q = TemporalDifference(alpha=alpha)
    ms: Sarsa = Sarsa(wrap, epsilon=epsilon, gamma=gamma, q=q)
    ms.reset()
    for _i in range(length):
        if _i % int(length / 10) == 0:
            print('+', end='', flush=True)
        elif _i % int(length / 100) == 0:
            print('.', end='', flush=True)
        ms.execute_one_action()
    print()
    return wrap.get_reward_summary(gamma), "SARSA with epsilon={}".format(epsilon)

if __name__ == "__main__":
    read: bool = False
    write: bool = False
    prefix: str = "./"

    if len(sys.argv) > 1:
        if sys.argv[1] == 'r':
            read = True
        if sys.argv[1] == 'w':
            write = True
    if len(sys.argv) > 2:
        prefix = sys.argv[2]
    
    domain: RLEnvironment = cheating_cricket_team_2()
    alpha: float = .05
    gamma = .99
    epsilon = .05
    
    data: List[Tuple[List[float],str]] = []
    DEFAULT_DURATION = 500000

    datum: Tuple[List[float],str] = ([],"Zero")
    for _i in range(DEFAULT_DURATION):
        datum[0].append(0)
    data.append(datum)

    filename = prefix + "q2_1"
    if read: 
        datum = read_rewards(filename)
    else:
        datum = basic_sarsa(domain, alpha=alpha, gamma=gamma, epsilon=epsilon, length=DEFAULT_DURATION)
        if write:
            write_rewards(filename, datum)
    data.append(datum)

    filename = prefix + "q2_2"
    if read: 
        datum = read_rewards(filename)
    else:
        datum = basic_qlearning(domain, alpha=alpha, gamma=gamma, epsilon=epsilon, length=DEFAULT_DURATION)
        if write:
            write_rewards(filename, datum)
    data.append(datum)

    print("Average discounted reward:")
    print("---------------+------------+------------+")
    print("Algorithm:     | SARSA      | Q-Learning |")
    print("---------------+------------+------------+")
    for i in range(10):
        sum1 = 0
        sum2 = 0
        steps = int(len(data[1][0])/10)
        for j in range(steps):
            sum1 += data[1][0][ (i*steps) + j ]
            sum2 += data[2][0][ (i*steps) + j ]
        print("      {:2} 10th: | {:10.4f} | {:10.4f} |".format((i+1), sum1/steps, sum2/steps))
    print("---------------+------------+------------+")

    from plot import plot_rewards
    plot_rewards(data)
    
'''
  Explain here why SARSA performs better than Q-learning during the learning.

  This is the same as the previous cricket domain except for the 10% chance that the cricket ball returns extra fast.
  This obviously makes it much harder to cheat successfully which is part the reason Q-Learning performs so poorly.
  While we can see that over time the SARSA algorithm learns that it is more profitable to play conservatively and avoid
  cheating, the Q-Learning algorithm still only see the direct benefit of cheating (because of the off policy learning),
  and that is why we can see it is consistently reaching scores between -100:-200.
'''

# eof